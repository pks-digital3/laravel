<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Detail Film</title>
</head>
<body>
    <h2>Detail Film</h2>

    @if($film)
        <p>Judul: {{ $film->judul }}</p>
        <p>Ringkasan: {{ $film->ringkasan }}</p>
        <p>Tahun: {{ $film->tahun }}</p>
        <p>Poster: {{ $film->poster }}</p>
        <p>Genre: {{ $film->genre->nama }}</p>
        <a href="{{ route('film.tampil', ['film_id' => $film->id]) }}">kembali ke halaman sebelumnya</a>
    @else
        <p>Data pemain film tidak ditemukan.</p>
    @endif
</body>
</html>