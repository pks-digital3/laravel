@extends('layout.master')

@section('judul')
Data Film
@endsection

@section('content')
 
<form action="/film/{{ $film->id  }}/update" method="post">       
        @csrf
        @method('PUT')
        <label>Judul:</label> <br> <br>
        <input type="text" name="judul"> ,<br> <br> 
        @error('judul')
        <div class="alert alert-danger">
        {{ $message }}
        </div>
        @enderror
        <label>Ringkasan:</label> <br> <br>
        <textarea name="ringkasan" id="" cols="30" rows="10"></textarea> <br>
        @error('ringkasan')
        <div class="alert alert-danger">
        {{ $message }}
        </div>
        @enderror
        <label>Tahun :</label> <br> <br>
        <input type="number" name="tahun"> ,<br> <br>
        @error('tahun')
        <div class="alert alert-danger">
        {{ $message }}
        </div>
        @enderror
        <label>Genre :</label> <br> <br>
        <input type="number" name="genre_id"> ,<br> <br>
        @error('genre_id')
        <div class="alert alert-danger">
        {{ $message }}
        </div>
        @enderror
        <input type="submit" value="Update">

@endsection