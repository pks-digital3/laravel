@extends('layout.master')

@section('judul')
Halaman Home
@endsection

@section('content')
    <h1>Media Online</h1>
    <h2>Sosial Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h2>Benefit belajar di media online</h2>
    <ul>
        <li>Mendapatkan motivasi dari sesama Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h2>Cara bergabung ke media online</h2>
    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftarkan di <a href="/daftar">Go to Register</a></li>
        <li>Selesai</li>
    </ol>
@endsection