
@extends('layout.master')

@section('judul')
List Genre Film
@endsection

@section('content')
 
 
    <form action="{{ route('genre.store') }}" method="post">
        @csrf
        <label>Nama :</label> <br> <br>
        <input type="text" name="nama"> ,<br> <br> 
        @error('nama')
        <div class="alert alert-danger">
        {{ $message }}
        </div>
        @enderror

        <input type="submit" value="Tambah">

@endsection