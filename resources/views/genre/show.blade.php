<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>List Genre Film</title>
</head>
<body>
    <h2>List Genre Film</h2>

    @if($genre)
        <p>Nama: {{ $genre->nama }}</p>
        <p>Films:</p>
        <ul>
            @foreach($genre->films as $film)
                <li>{{ $film->judul }} - {{ $film->tahun }}</li>
            @endforeach
        </ul>
        <a href="{{ route('genre.index', ['genre_id' => $genre->id]) }}">kembali ke halaman sebelumnya</a>
    @else
        <p>Data pemain film tidak ditemukan.</p>
    @endif
</body>
</html>