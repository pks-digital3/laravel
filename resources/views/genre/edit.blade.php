@extends('layout.master')

@section('judul')
List Genre Film
@endsection

@section('content')
 
    <form action="{{ route('genre.update', ['genre_id' => $genre->id]) }}" method="post">
        @csrf
        @method('PUT')
        <label>Nama :</label> <br> <br>
        <input type="text" name="nama"> ,<br> <br> 
        @error('nama')
        <div class="alert alert-danger">
        {{ $message }}
        </div>
        @enderror

       
        <input type="submit" value="Update">

@endsection