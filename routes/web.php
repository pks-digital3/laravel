<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'home']);
Route::get('/daftar', [AuthController::class, 'register']);
Route::post('/kirim', [AuthController::class, 'kirim']);
Route::get('/data-table', [IndexController::class, 'dataTable']);




Route::get('/cast', [CastController::class, 'index'])->name('cast.index');
Route::get('/cast/create', [CastController::class, 'create'])->name('cast.create');
Route::post('/cast/create', [CastController::class, 'store'])->name('cast.store');
Route::get('/cast/{cast_id}', [CastController::class, 'show'])->name('cast.show');
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'])->name('cast.edit');
Route::put('/cast/{cast_id}', [CastController::class, 'update'])->name('cast.update');
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy'])->name('cast.destroy');

Route::get('/film', [FilmController::class, 'tampil'])->name('film.tampil');
Route::get('/film/create', [FilmController::class, 'create'])->name('film.create');
Route::post('/film/create', [FilmController::class, 'store'])->name('film.store');
Route::get('/film/{film_id}', [FilmController::class, 'show'])->name('film.show');
Route::get('/film/{film_id}/edit', [FilmController::class, 'edit'])->name('film.edit');
Route::put('/film/{film_id}/update', [FilmController::class, 'update'])->name('film.update');
Route::delete('/film/{film_id}', [FilmController::class, 'destroy'])->name('film.destroy');

Route::get('/genre', [GenreController::class, 'index'])->name('genre.index');
Route::get('/genre/create', [GenreController::class, 'create'])->name('genre.create');
Route::post('/genre/create', [GenreController::class, 'store'])->name('genre.store');
Route::get('/genre/{genre_id}', [GenreController::class, 'show'])->name('genre.show');
Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit'])->name('genre.edit');
Route::put('/genre/{genre_id}', [GenreController::class, 'update'])->name('genre.update');
Route::delete('/genre/{genre_id}', [GenreController::class, 'destroy'])->name('genre.destroy');

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
